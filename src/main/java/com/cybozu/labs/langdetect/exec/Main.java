/*
 * Copyright (c) 2014 3CSI All Rights Reserved
 *
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF 3CSI
 * The copyright notice above does not evidence any actual or intended
 * publication of such source code.
 */
package com.cybozu.labs.langdetect.exec;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

import java.io.File;

public class Main
{
    public static final String PROFILES = "C:/users/daniel/dev/work/language-detection/profiles";
    public static void main(String[] args)
    {
        String text = args[0];
        try
        {
            DetectorFactory.loadProfile(new File(PROFILES));
            Detector detector = DetectorFactory.create();
            detector.setMaxTextLength(1024);
            detector.append(text);
            String lang = detector.detect();
            System.out.println("Lang: " + lang);
        } catch (LangDetectException e)
        {
            e.printStackTrace();
        }
    }
}
